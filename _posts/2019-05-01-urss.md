---
title: Unidad Recargable Sexy y Sustentable (URSS)
layout: post
---

![Pack de baterías con módulo de descarga regulable
MT3608](assets/images/pack_con_descarga.jpg)

# Materiales

* Batería de notebook en cualquier estado de desuso (de las 6 baterías
  que puede contener, algunas están rotas, otras no). Armamos packs de
  a 3. También se pueden usar de celular, mientras sean de 3.7V.

* Cables de distintos colores (reciclados de una fuente de PC rota)

* Conectores Mólex (los sacamos de las fuentes de PC)

* Módulo de carga TP4056

* Módulo booster DC 5v

* Módulo step up MT3608

* Cables USB-miniUSB

* Contenedores bonitos (tuppers, cajitas de tic tac, cualquier cosa
  plástica que proteja y aisle)

* Cinta aisladora

* Termocontraíble

# Herramientas necesarias

* Alicate

* Destornillador

* Soldadora

* Estaño

* Apoya soldadora (puede ser un cacho de alambre, una lata, cualquier
  cosa que no se derrita y que sostenga la soldadora)

* Fibra indeleble (o marcador permanente)

# Procedimiento

## Rescatar las baterías

Rescatamos las baterías que no funcionan de las notebooks.  La carcaza
está sellada y no hay forma de abrirla.  Nosotrxs usamos una morsa, un
cincel (en realidad un destornillador plano grande) y un martillo.
Puede ser con una dremel con disco de corte o con sierra de arco. La
mejor forma que encontramos fue empezar a cortar por la costura de la
carcaza e ir abriendo por el costado.  Las baterías están muy pegadas al
plástico y si no trabajamos con cuidado las podemos pinchar.

![Pila de baterías de celular y notebook de distintos colores](assets/images/baterias.jpg)

Una vez abierta, van a encontrar 6 baterías 18650 unidas por chapitas
metálicas y un circuito que es el controlador de carga.  El circuito
todavía no sabemos cómo reciclarlo y por lo general es lo que se arruina
mientras las baterías siguen funcionando, así que por ahora lo guardamos
(también tiene uno o varios sensores térmicos).  Las chapitas metálicas
se pueden cortar para separar las baterías, pero no conviene cortarlas
del todo porque es más fácil soldar los cables luego.  Si la chapa se
despega de la batería, para poder soldar luego hay que limar la batería
para que el estaño agarre.


Las baterías vienen de distintos colores (que no significan nada, pero
qué lindos, ¿no?), con lo que podemos combinar luego, siempre teniendo
cuidado de que tengan el mismo voltaje.

Necesitaremos ver cuáles funcionan y cuáles no.

Para eso, con el tester medimos el voltaje de cada una: tiene que ser
mayor a 2 voltios (aunque algunas publicaciones recomiendan entre 3,5 y
4 V). Con una fibra indeleble escribimos el voltaje en la misma batería.
Las baterías de descarte se pueden guardar para enviar a reciclar
(estimamos que las tiran en el camino del Buen Ayre...)


## Armar el pack de baterías

Ubicamos 3 pilas "acostadas" (ver imagen). Vamos a conectarlas en serie:
los tres lados positivos juntos por un lado y los tres negativos juntos
por el otro.  Esto se hace soldando los cables a la base de las pilas
(ver imagen). Recomendamos usar colores que permitan la identificacion
como negro para negativo y rojo u otro color para positivo. Si dejamos
las chapitas pegadas es más fácil soldar, si las sacamos o se salieron
hay que limar un poco las terminales de la batería para que el estaño
agarre.

Mantenemos las pilas unidas mediante una cinta. Si se les ocurre un
metodo mas sexy, pueden probarlo y sumarlo a la documentación.

Unimos los cables (nosotres los soldamos cable con cable) a los de un
molex pinchudo. Sirven los reciclados de las fuentes viejas de
computadora. (ver imagen)

El molex pinchudo se conecta con el molex hoyudo.  Los mólex pinchudos
son más fáciles de conseguir porque vienen en la fuente de la PC.  Los
mólex hoyudos son más difíciles y se consiguen en las extensiones de
cables y en los _coolers_ (los ventiladores).  Como tienen 4 pines y
vamos a usar solo 2, elegimos el par rojo-negro y cancelamos el par
amarillo-negro cortándolos al ras.  Usamos estos conectores porque los
comprados son difíciles de armar, necesitan herramientas especiales y
caras y manualmente nunca quedan bien.  Si se les ocurren conectores
reciclados de algún dispositivo/cable fácilmente conseguible (por
ejemplo, no los USB porque los hoyudos son más difíciles), avisen!

Entonces, soldamos el cable positivo del pack de baterías al rojo del
mólex y el negativo al negro. Para que quede prolijo, usamos tubitos
termocontraíbles.  El proceso es:

* Cortar 2 tubitos de termocontraíble de 1-2cm y pasarlos por los cables
  del pack de baterías

* Pelar los cables del pack

* Cortar el par amarillo y negro del mólex pinchudo al ras para
  cancelarlos

* Cortar el par rojo y negro del mólex pinchudo al menos a 3cm y
  pelarlos

* Estañar las 4 puntas (estañar es pintar con estaño los cables antes de
  soldarlos para que solo haya que unirlos después)

* Soldar el positivo del pack con el rojo del mólex y el negativo con el
  negro (dar un tironcito a cada uno para probar que hayan quedado bien
  unidos)

* Una vez soldados los cables, deslizar los termocontraíbles para cubrir
  las partes soldadas y con el borde de la soldadora (no la punta!)
  pasar suavemente por el termocontraíble para que se contraiga.

Ya está listo el pack de baterías!  El pack tiene una salida igual al
voltaje individual de las baterías (nominalmente 3.7V) y una intensidad
de corriente combinada (nominalmente 1.5A * 3 baterías = 4.5A)

## Armar el módulo de carga

Para armar el módulo de carga, seguir los mismos pasos para soldar
cables a un conector mólex, pero esta vez en lugar de pinchudo es
hoyudo.  Una vez terminado este paso, hay que soldar los cables a las
terminales del módulo TP4056.  Soldados el cable positivo (el rojo del
mólex) a la terminal positiva del TP4056 y el negativo a la terminal
negativa.

El módulo de carga permite cargar el pack de baterías utilizando un
cargador común de celular.  En nuestras pruebas, tarda 5 horas en
terminar de cargar.  Tiene un LED rojo para indicar que está cargando y
uno verde para indicar que la carga está completa.

![Pack de baterías con módulo de carga TP4056](assets/images/pack_con_carga.jpg)

## Armar el módulo de descarga

Hacemos los mismos pasos que el módulo de carga, con mólex pinchudo
incluso, en el módulo Booster DC 5V.  Este módulo permite cargar un
celular o una Raspberry, aunque el voltaje de salida es de 5V y la
potencia baja de 4.5A a 0.6A (600mA), con lo que una Raspberry puede
estar muy justa para alimentarse.

## Armar el módulo de descarga regulable

Como el módulo booster 5V es bastante limitado (aunque tiene un conector
USB hoyudo!) en cuanto a potencia y voltaje y además un poco caro en
comparación a otros módulos, usamos un módulo step up MT3608 que permite
un voltaje de entrada de 3.7V y es capaz de elevarlo hasta 28V!  Además,
puede entregar 2A, con lo que podemos alimentar dispositivos más
hambrientos.

Para la parte de entrada, marcadas en el módulo como VIN+ y VIN-
(positivo y negativo), repetimos los pasos para el módulo de descarga y
de carga normal.

Para la parte de salida, marcadas como VOUT+ y VOUT-, volvemos a usar un
mólex pinchudo y lo soldamos como en el pack de baterías.

Con esto podemos armar conectores de descarga usando un mólex hoyudo y
el conector que queramos, por ejemplo un plug de 2.1mm para alimentar
routers TP-Link, un conector mini-USB pinchudo para alimentar
Raspberries, etc.

Es importante tener cuidado con el voltaje o podemos quemar las
dispositivas!  Para alimentar un router usando el módulo jack 2.1mm, hay
que regular el MT3608 en 9V.  Para una Raspberry, hay que regularlo en
5.1V.

### Regular el modulo

Con un destornillador plano chiquito y un téster podemos regular el
MT3608.

* Conectar la batería al módulo para alimentarle voltaje

* Medir voltaje con el téster en las terminales VOUT del módulo

* Con el destornillador ir girando el tornillito del cosito azul del
  módulo hasta que logramos el voltaje que queremos.  A veces toma
  muchas vueltas en empezar a regular, hay que tenerle paciencia.

* Podemos escribir el voltaje con un marcador indeleble aunque siempre
  conviene testear antes de usarlo por si se mueve solo (o alguien lo
  movió)

## Proteger el pack y los módulos

Para proteger el pack y los módulos podemos usar:

* Tuppers chiquitos para el pack

* Cajitas de tictac para los módulos (aunque no son muy resistentes,
  tienen una puertita para sacar los cables)

* Carcasas recicladas de viejas dispositivas ya difuntas

* ...

* Cajitas de cartón (?)







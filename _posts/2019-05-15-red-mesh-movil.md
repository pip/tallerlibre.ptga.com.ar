---
title: Redes mesh móviles para cobertura y comunicacion de medios alternativos dentro del movimiento feminista
author: f@sutty.nl
layout: post
---

<video
controls
poster="assets/images/encendiendo_un_router_con_bateria.jpg"
src="assets/videos/encendiendo_un_router_con_bateria.webm"></video>

El componente tecnológico que estamos desarrollando es un grupo de nodos
para el despliegue de redes mesh móviles, conformada por routers
comerciales de bajo costo utilizando el firmware LibreMesh, con antenas
de mayor ganancia y pack de baterías para darles autonomía. Pueden ser
colocados en lugares fijos o cargados por personas.  Cuando dos o más
nodos están al alcance forman una red que se extiende a medida que se
agregan más; si uno de ellos tiene acceso a Internet toda la red pasa a
tener conectividad.

El pack de baterías recargables fue desarrollado por nosotres reciclando
baterías de notebooks y celulares en desuso y cuentan con una autonomía
estimada de 4 horas.

Estamos terminando unos diez nodos y preparándonos para hacer las
primeras pruebas en territorio. El proceso incluye la documentación de
la red, para poder generar material de difusión que facilite la
apropiación y replicación de la red por otras organizaciones y contextos
(por ejemplo, desastre ambiental).

Creemos que nuestro desafío e innovación es adaptar el desarrollo
existente de las redes mesh comunitarias en contextos mayormente rurales
a contextos urbanos de movilización masiva. El impacto social que
buscamos es poder informar masivamente, a la vez que generar una
herramienta de comunicación interna para garantizar la integridad física
les comunicadores.

El acceso a Internet es clave para la cobertura en tiempo real de las
luchas feministas, tan masivas en los últimos años que saturan la
conectividad celular por la sola cantidad de personas y organizaciones
movilizadas. Esto dificulta la tarea de cobertura de los medios
alternativos y comunitarios, que no pueden cubrir en vivo. Los medios
hegemónicos tienen la capacidad de informar en vivo, imponiendo su
perspectiva patriarcal e invisibilizando las luchas y reclamos,
detenciones arbitrarias y situaciones represivas, que atentan contra la
libertad de prensa.  [Argentina está en el puesto 57 de la clasificación
de libertad de prensa elaborada por Reporteros Sin Fronteras, cinco
posiciones por debajo en relación al año anterior
](https://rsf.org/es/argentina).

Nuestro objetivo a mediano plazo es incorporar herramientas de software
libre para comunicación interna y replicación/sincronización del
material recolectado, de forma que sirvan como memoria colectiva y
evidencia en casos de vulneración de derechos humanos.
